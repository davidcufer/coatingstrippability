﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CoatingStrippability
{
    /// <summary>
    /// Interaction logic for Properties.xaml
    /// </summary>
    public partial class PropertiesWindow : Window
    {
        public delegate void TaraSet(object sender, TaraArgs e);
        public event TaraSet OnTaraPressed;


        public PropertiesWindow(bool connected)
        {

            InitializeComponent();
            button_tara.IsEnabled = connected;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
         }

        private void button_Click_TaraOut(object sender, RoutedEventArgs e)
        {
            TaraArgs args = new TaraArgs(true);
            OnTaraPressed(this, args);
        }

     }

    public class TaraArgs : EventArgs
    {
        public bool Tara { get; private set; }

        public TaraArgs(bool tara)
        {
            Tara = tara;
        }
    }
}
