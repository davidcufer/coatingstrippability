﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwinCAT.Ads;
using System.IO;
using System.Timers;
using System.Collections;
using System.Threading;
using System.Diagnostics;
using CoatingStrippability.Settings;

namespace CoatingStrippability
{
    public class CommModul
    {
        private AdsStream dataStream;
        private AdsStream dataStream1;
        private AdsBinaryReader binRead1;

        private TcAdsClient tcClient;
        private AdsBinaryReader binRead;
        private System.Timers.Timer aTimer = new System.Timers.Timer();
        public bool TwincatConnected { get; set; }
        public delegate void ConnectionChangedHandler(object sender, ConnectionChangedArgs e);
        public event ConnectionChangedHandler OnConnectionChanged;
        private int hStartRec;
        private int hStopRec;
        private int hForceArray;
        private int hTimeArray;
        private int hTara;
        private int hForce;

        public CommModul()
        {
            dataStream = new AdsStream(64000*4);
            binRead = new AdsBinaryReader(dataStream);
            tcClient = new TcAdsClient();

            dataStream1 = new AdsStream(4);
            binRead1 = new AdsBinaryReader(dataStream1);

        }

        public void Connect()
        {

            try
            {
                tcClient.ConnectionStateChanged += TcClient_ConnectionStateChanged;

                tcClient.Connect(MySettings.Load().NetID, 801);

                hStartRec = tcClient.CreateVariableHandle("MAIN.StartRecording");
                hStopRec = tcClient.CreateVariableHandle("MAIN.StopRecording");
                hTara= tcClient.CreateVariableHandle("MAIN.TaraIn");
                hForce = tcClient.CreateVariableHandle("MAIN.ForceOut");

            }
            catch 
            {

            }

            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = 1000;
            aTimer.Enabled = true;
        }

        public bool StartAcquisition()
        {
           // data.Clear();
          //  dataTime.Clear();

            if (tcClient.IsConnected)
            {
              //  hdint1 = tcClient.CreateVariableHandle("MAIN.ForceOut");

                hForceArray = tcClient.CreateVariableHandle("MAIN.ForceArray");
                hTimeArray = tcClient.CreateVariableHandle("MAIN.TimeArray");

                //  hConnect = tcClient.AddDeviceNotificationEx("MAIN.ForceOut", AdsTransMode.Cyclic, 1, 0, hdint1, typeof(int));

                //  tcClient.AdsNotification += OnNotification;
                return true;
            }

            else return false;
        }

        public int GetForce()
        {
            int force = 0;

            try
            {
                tcClient.Read(hForce, dataStream1);

                dataStream1.Position = 0;

                force = binRead1.ReadInt32();
            }
            catch
            {

            }

            return force;
        }

        public Tuple<List<TimeSpan>, List<Double>> StopAcquisition()
        {
            //   tcClient.AdsNotification -= OnNotification;


            Stopwatch a=new Stopwatch();
            a.Start();
            Tuple<List<TimeSpan>, List<double>> DataToReturn;



            tcClient.Read(hTimeArray, dataStream);
            dataStream.Position = 0;
            List<TimeSpan> test1 = new List<TimeSpan>();
   
            while (dataStream.Position != dataStream.Length)
            {
                test1.Add(binRead.ReadPlcTIME());
            }


            dataStream.Position = 0;

            tcClient.Read(hForceArray, dataStream);

            List<int> test = new List<int>();

            dataStream.Position = 0;
            while (dataStream.Position != dataStream.Length)
            {
                test.Add(binRead.ReadInt32());
            }

            List<double> testScaled = new List<double>();

            double scaling = MySettings.Load().Scaling;

            for (int i = 0; i < test.Count; i++)
            {
                testScaled.Add((double)test[i] / scaling);
            }

            DataToReturn = new Tuple<List<TimeSpan>, List<double>>(test1, testScaled);
            a.Stop();

            return DataToReturn;
        }


        private void TcClient_ConnectionStateChanged(object sender, TwinCAT.ConnectionStateChangedEventArgs e)
        {

        }

        public void WriteBoolToPLC(bool start)
        {
            if (start)
            {

                tcClient.WriteAny(hStopRec, false);
                tcClient.WriteAny(hStartRec, true);

            }
            else
            {
                tcClient.WriteAny(hStartRec, false);
                tcClient.WriteAny(hStopRec, true);
            }
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            StateInfo info;
            try
            {
                tcClient.TryReadState(out info);

                if (info.AdsState == AdsState.Run)
                {
                    ConnectionChangedArgs args = new ConnectionChangedArgs(true);
                    OnConnectionChanged(this, args);
                    TwincatConnected = true;
                }
                else
                {
                    ConnectionChangedArgs args = new ConnectionChangedArgs(false);
                    OnConnectionChanged(this, args);

                    TwincatConnected = false;
                }
            }
            catch { }

        }

        public void WriteTara()
        {
            tcClient.WriteAny(hTara, true);
            Thread.Sleep(100);
           tcClient.WriteAny(hTara, false);

        }
        /*   private void OnNotification(Object sender, AdsNotificationEventArgs e)
           {
               if (e.NotificationHandle == hConnect)
               {
                   var test = tcClient.ReadAny(hdint1, typeof(Int32)).ToString();
                   data.Add(Int32.Parse(test));
                   dataTime.Add(DateTime.Now);
               }
           }*/
    }

    public class ConnectionChangedArgs : EventArgs
    {
        public bool Connected { get; private set; }

        public ConnectionChangedArgs(bool connected)
        {
            Connected = connected;
        }
    }
}
