﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Threading; // For Dispatcher.
using OxyPlot;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Timers;
using System.Diagnostics;
using CoatingStrippability.Settings;
using System.Data;

namespace CoatingStrippability
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private MainWindowModel model;
        public PlotModel TimePlot { get; set; }
        private bool connected;
        public ButtonColor ConnectButtonColor { get; set; }
        private double _currentForce;
        //public List<Results> Results { get; set; }
        public DataTable _resultsTable = new DataTable("resultstable");
        public DataTable _meanTable = new DataTable("meantable");
        private int measurementindex = 0;


        private Lloyd lloyd = new Lloyd();
        System.Timers.Timer moveTimer = new System.Timers.Timer();
        System.Timers.Timer acqTimer = new System.Timers.Timer();
        public DataTable ResultsTable
        {
            get
            {
                return _resultsTable;
            }
            set
            {
                _resultsTable = value;
                OnPropertyChanged("ResultsTable");
            }
        }
        public DataTable MeanTable
        {
            get
            {
                return _meanTable;
            }
            set
            {
                _meanTable = value;
                OnPropertyChanged("MeanTable");
            }
        }
        public double CurrentForce
        {
            get
            {
                return _currentForce;
            }
            set
            {
                _currentForce = value;
                OnPropertyChanged("CurrentForce");
            }
        }
    
        public MainWindow()
        {
            InitializeComponent();
            
            this.SetUpThings();
            model.PropertyChanged += Model_PropertyChanged;
            acqTimer.Elapsed += StopAcquisition;
            moveTimer.Elapsed += StopReturn;
            this.InitializeTables();
        }

        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("CurrentForce"))
            {
                CurrentForce = model.CurrentForce;
            }
        }

        private void SetUpThings()
        {

            ConnectButtonColor = new ButtonColor();
            ConnectButtonColor.ConnectButtonColor = Brushes.IndianRed;
            model = new MainWindowModel();
            this.DataContext = this;

            model.ADS.OnConnectionChanged += Model_OnConnectionChanged;
            button_Start.IsEnabled = false;
            TimePlot = model.Plot1.TimePlot;

            //Results = new List<Results>();

        }

        private void Button_Properties_Click_1(object sender, RoutedEventArgs e)
        {
            PropertiesWindow win2 = new PropertiesWindow(connected);
            win2.OnTaraPressed += model.Model_OnTaraPressed;
            win2.Show();
        }

        private void button_Connect_Click(object sender, RoutedEventArgs e)
        {
            model.ConnectToADS();
            lloyd.Initialize();
           /* Results.Add(new Results()
            {
                Index = 0,
                Max = 100
            });
            Results.Add(new Results()
            {
                Index = 1,
                Max = 12
            });*/
        
    }

        private void CommonCommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Model_OnConnectionChanged(object sender, ConnectionChangedArgs e)
        {
            if (e.Connected)
            {
                connected = true;
                ConnectButtonColor.ConnectButtonColor = Brushes.LightGreen;

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => this.button_Start.IsEnabled = true));
            }
            else
            {
                connected = false;
                ConnectButtonColor.ConnectButtonColor = Brushes.IndianRed;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => this.button_Start.IsEnabled = false));
            }
        }

        public void ReturnDown()
        {
            var TestType = MySettings.Load().TestType;
            lloyd.StartJogDown();
            if (TestType == TestType.Strippability)
            {
                moveTimer.Interval = 7500;
            }
            else
            {
                moveTimer.Interval = 1080;
            }
            moveTimer.AutoReset = false;
            moveTimer.Start();

        }

        public void JogUp(object sender,System.Windows.Input.MouseEventArgs e)
        {
            lloyd.JogUp();
        }

        public void JogDown(object sender, System.Windows.Input.MouseEventArgs e)
        {
            lloyd.JogDown();
        }

        public void JogStop(object sender, System.Windows.Input.MouseEventArgs e)
        {
            lloyd.StopMovement();
        }


        void StopReturn(Object source, ElapsedEventArgs e)
        {
            lloyd.StopMovement();
            moveTimer.Stop();
        }


        void StopAcquisition(Object source, ElapsedEventArgs e)
        {
            lloyd.StopMovement();
            var result = model.StopAcquisition();
            acqTimer.Stop();
            AgregateResults(result);
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => { button_Start.Content = "Start"; OnPropertyChanged("ResultsTable"); }));
            
        }

        private void button_Start_Click(object sender, RoutedEventArgs e)
        {
            if (button_Start.Content.Equals("Start"))
            {
                if (model.StartAcquisition())
                {
                    button_Start.Content = "Stop";
                    if (MySettings.Load().TestType == TestType.Strippability)
                    {
                        acqTimer.Interval = 52000;
                        lloyd.SetSpeed(50.0);
                    }
                    else
                    {
                        acqTimer.Interval = 35000;
                        lloyd.SetSpeed(10.0);
                    }

                    if (lloyd.StartSlowUp())
                    {
                        acqTimer.Start();
                        acqTimer.AutoReset = false;
                    }
                }
            }
            else
            {
                lloyd.StopMovement();
                var result = model.StopAcquisition();
                AgregateResults(result);

                button_Start.Content = "Start";
            }
        }

        private void button_Return_Click(object sender, RoutedEventArgs e)
        {
            ReturnDown();
        }

        private void AgregateResults(double[] result)
        {
            var row = ResultsTable.NewRow();

            row[0]= this.measurementindex;
            for (int i = 1; i< result.Length+1; i++) 
            {
                row[i] = Math.Round(result[i-1], 2); //row[i] goes through each column
            }

            ResultsTable.Rows.Add(row);

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => this.dataGridResults.Items.Refresh()));

            this.UpdateMeanTable();

            this.measurementindex++;




            TimePlot = model.Plot1.TimePlot;
            OnPropertyChanged("TimePlot");
        }

        private void InitializeTables()
        {

            ResultsTable = new DataTable();

            if (MySettings.Load().TestType == TestType.Strippability)
            {
                ResultsTable.Columns.Add("Index", typeof(int));
                ResultsTable.Columns.Add("Max", typeof(double));
                ResultsTable.Columns.Add("Min", typeof(double));
                ResultsTable.Columns.Add("Median", typeof(double));
                ResultsTable.Columns.Add("FirstPeak", typeof(double));
                ResultsTable.Columns.Add("StDev", typeof(double));
            }
            else
            {
                ResultsTable.Columns.Add("Index", typeof(int));
                ResultsTable.Columns.Add("Max", typeof(double));
                ResultsTable.Columns.Add("Min", typeof(double));
                ResultsTable.Columns.Add("Median", typeof(double));
                ResultsTable.Columns.Add("FirstPeak", typeof(double));
                ResultsTable.Columns.Add("StDev", typeof(double));
            }
            dataGridResults.DataContext = ResultsTable.DefaultView;

            MeanTable = new DataTable();
            MeanTable.Columns.Add("Parameter", typeof(string));
            MeanTable.Columns.Add("Value", typeof(double));
            meanGridResults.DataContext = MeanTable.DefaultView;

        }
        private void UpdateMeanTable()
        {

            MeanTable.Clear();


            if (MySettings.Load().TestType == TestType.Strippability)
            {

                var meanResults = ProcessStrippability.CalculateMean(ResultsTable);

                var row = MeanTable.NewRow();

                row[0] = "Max";
                row[1] = Math.Round(meanResults[1], 2);
                MeanTable.Rows.Add(row);

                row = MeanTable.NewRow();
                row[0] = "Min";
                row[1] = Math.Round(meanResults[2], 2);
                MeanTable.Rows.Add(row);

                row = MeanTable.NewRow();
                row[0] = "Median";
                row[1] = Math.Round(meanResults[3], 2);
                MeanTable.Rows.Add(row);

                row = MeanTable.NewRow();
                row[0] = "FirstPeak";
                row[1] = Math.Round(meanResults[4], 2);
                MeanTable.Rows.Add(row);

                row = MeanTable.NewRow();
                row[0] = "StDev";
                row[1] = Math.Round(meanResults[5], 2);
                MeanTable.Rows.Add(row);
            }
            else
            {
                var meanResults = ProcessStrippability.CalculateMean(ResultsTable);

                var row = MeanTable.NewRow();

                row[0] = "Max";
                row[1] = Math.Round(meanResults[1], 2);
                MeanTable.Rows.Add(row);

                row = MeanTable.NewRow();
                row[0] = "Min";
                row[1] = Math.Round(meanResults[2], 2);
                MeanTable.Rows.Add(row);

                row = MeanTable.NewRow();
                row[0] = "Median";
                row[1] = Math.Round(meanResults[3], 2);
                MeanTable.Rows.Add(row);

                row = MeanTable.NewRow();
                row[0] = "FirstPeak";
                row[1] = Math.Round(meanResults[4], 2);
                MeanTable.Rows.Add(row);

                row = MeanTable.NewRow();
                row[0] = "StDev";
                row[1] = Math.Round(meanResults[5], 2);
                MeanTable.Rows.Add(row);
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => this.meanGridResults.Items.Refresh()));
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }


        private void button_ClearTable_Click(object sender, RoutedEventArgs e)
        {
            this.InitializeTables();
            this.measurementindex = 0;
        }

        private void dataGridResults_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                var indexToRemove = this.dataGridResults.SelectedIndex;
                this.ResultsTable.Rows[indexToRemove].Delete();
                this.dataGridResults.Items.Refresh();
                this.UpdateMeanTable();
                
            }
        }
      
        private void Button_Settings_Click_1(object sender, RoutedEventArgs e)
        {
            var SettingsWindow = new Settings.Settings();
            SettingsWindow.Show();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }



    public class ButtonColor : INotifyPropertyChanged
    {
        Brush _connectButtonColor;

        public Brush ConnectButtonColor
        {
            get
            {
                return _connectButtonColor;
            }
            set
            {
                _connectButtonColor = value;
                OnPropertyChanged("ConnectButtonColor");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
