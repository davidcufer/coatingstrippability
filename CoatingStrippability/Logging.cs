﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CoatingStrippability.Settings;

namespace CoatingStrippability
{
    class Logging
    {
        public Logging()
        {

        }

       public void WriteToFile(Tuple<List<TimeSpan>, List<double>> data)
        {
            if (!string.IsNullOrEmpty(MySettings.Load().LoggingPath.ToString()))
                {
                var folder = MySettings.Load().LoggingPath;
                var filepath = folder.PathString + "\\CoatStrip_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss.fff") + ".csv";
                try
                {
                    using (StreamWriter writer = new StreamWriter(new FileStream(filepath, FileMode.Create, FileAccess.Write)))
                    {
                        writer.WriteLine("{0},{1}", "Time(ss.ms)", "Force");

                        for (int i = 0; i < data.Item1.Count; i++)
                        {
                            int n = 3 - data.Item1[i].Milliseconds.ToString().Length;

                            string column1 = data.Item1[i].Seconds.ToString() + "." + new String('0', n) + data.Item1[i].Milliseconds.ToString();

                            writer.WriteLine("{0},{1}", column1, ((double)data.Item2[i]).ToString("F3"));
                        }
                    }
                }
                catch
                {

                }
            }
        }
    }
}
