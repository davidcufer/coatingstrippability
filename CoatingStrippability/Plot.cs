﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using System.Windows.Threading;
using System.Globalization;
using CoatingStrippability.Settings;

namespace CoatingStrippability
{
    public class Plot : INotifyPropertyChanged 
    {
        private Dispatcher m_userInterfaceDispatcher;

        public PlotModel _timePlot;
        public LineSeries Data { get; private set; }
        private TimeSpanAxis xAxisTime;

        public Plot()
        {
            m_userInterfaceDispatcher = Dispatcher.CurrentDispatcher;

            xAxisTime = new TimeSpanAxis
            {
                Position = AxisPosition.Bottom,
                StringFormat = "ss:ff",

                MajorStep = 0.5,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dash,
            };

            TimePlot = new PlotModel();
            TimePlot.Axes.Add(xAxisTime);
            TimePlot.Axes.Add(new LinearAxis { Position = AxisPosition.Left, IsZoomEnabled = true });

            Data = new LineSeries { Color = OxyColor.FromRgb(255, 92, 51), LineStyle = LineStyle.Solid, StrokeThickness = 1 };
            Data.Points.Add(new DataPoint(TimeSpanAxis.ToDouble(0), 0));

            TimePlot.Series.Add(Data);
        }

        public void Update(Tuple<List<TimeSpan>, List<double>> data)
        {

            Data.Points.Clear();
            for (int i = 0; i < data.Item1.Count(); i++)
            {
                if (i > 0 && data.Item1[i]==TimeSpan.Zero)
                {
                     double majorStep= Math.Round(2*((double)i)) /2;

                    xAxisTime.MajorStep = majorStep == 0 ? 0.5 : majorStep;
                    break;
                }

                Data.Points.Add(new DataPoint(TimeSpanAxis.ToDouble(data.Item1[i]), data.Item2[i]));
            }

            OnPropertyChanged("TimePlot");

            m_userInterfaceDispatcher.InvokeAsync(new Action(() =>
            {
                lock (this.TimePlot.SyncRoot)
                {

                    this.TimePlot.InvalidatePlot(true);

                   
                      foreach (var axis in TimePlot.Axes)
                            {
                                axis.Reset();
                            }
                        }
                        this.TimePlot.InvalidatePlot(true);
            }));
        }

        public PlotModel TimePlot
        {
            get
            {
                return _timePlot;
            }
            set
            {
                _timePlot = value;
                OnPropertyChanged("TimePlot");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
