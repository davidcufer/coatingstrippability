﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web.Script.Serialization;

namespace CoatingStrippability.Settings
{
    public class AppSettings<T> where T : new()
    {
        private const string DEFAULT_FILENAME = "settings.json";

        public void Save(string fileName = DEFAULT_FILENAME)
        {
            File.WriteAllText(fileName, (new JavaScriptSerializer()).Serialize(this));
        }

        public static void Save(T pSettings, string fileName = DEFAULT_FILENAME)
        {
            File.WriteAllText(fileName, (new JavaScriptSerializer()).Serialize(pSettings));
        }

        public static T Load(string fileName = DEFAULT_FILENAME)
        {
            try
            {
                T t = new T();
                if (File.Exists(fileName))
                    t = (new JavaScriptSerializer()).Deserialize<T>(File.ReadAllText(fileName));
                return t;
            }
            catch
            {
                T t = new T();
                return t;
            }
        }
    }

    public class MySettings : AppSettings<MySettings>
    {
        [PropertyName("Control Plan")]
        public FilePath DataByStations = new FilePath(@"\\skofs01.li.lumentuminc.net\11-Software\15-ControlPlan\10139523-068_Rev000_Control_Plan_PDPBohinj.xlsm");
        [PropertyName("Production mode")]
        public bool ProductionMode = false;
        [PropertyName("TestType")]
        public TestType TestType = TestType.Strippability;
        [PropertyName("Scaling")]
        public double Scaling = 1000;
        [PropertyName("Threshold")]
        public double Threshold = 1;
        [PropertyName("LoggingEnabled")]
        public bool LoggingEnabled = false;
        [PropertyName("LoggingPath")]
        public FilePath LoggingPath = new FilePath(@"\\skofs01.li.lumentuminc.net\04-PVK\50-Production\Lloyd");
        [PropertyName("NetID")]
        public string NetID = "5.57.66.15.1.1";
        [PropertyName("ComPort")]
        public string ComPort = "COM1";


    }

    public class PropertyName : Attribute
    {
        public PropertyName()
        {

        }

        public PropertyName(string _displayName)
        {
            DisplayName = _displayName;
        }

        public string DisplayName { get; set; }
    }

}
