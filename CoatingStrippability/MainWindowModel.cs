﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwinCAT.Ads;
using System.IO;
using System.ComponentModel;
using System.Timers;
using System.Windows.Media;
using OxyPlot;
using CoatingStrippability.Settings;


namespace CoatingStrippability
{
    class MainWindowModel : INotifyPropertyChanged
    {

        public CommModul ADS;
        public Plot Plot1 { get; set; }
        private System.Timers.Timer aTimer = new System.Timers.Timer();
        private bool acqStarted;
        private bool connectOnce = false;
        private double _currentForce;
        private List<TimeSpan> cleanedData;

        public double CurrentForce
        {
            get
            {
                return _currentForce;
            }
            set
            {
                _currentForce = value;
                OnPropertyChanged("CurrentForce");
            }
        }

        public MainWindowModel()
        {
            ADS = new CommModul();
            Plot1 = new Plot();
        }

        /*   public event PropertyChangedEventHandler PropertyChanged;
           protected virtual void OnPropertyChanged(string propertyName)
           {
               PropertyChangedEventHandler handler = PropertyChanged;
               if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
           }
           protected bool SetField<T>(ref T field, T value, string propertyName)
           {
               if (EqualityComparer<T>.Default.Equals(field, value)) return false;
               field = value;
               OnPropertyChanged(propertyName);
               return true;
           }*/


        public void ConnectToADS()
        {
            ADS.Connect();

            if (!connectOnce)
            {
                connectOnce = true;
                aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                aTimer.Interval = 400;
                aTimer.Enabled = true;
             }
        }  

        public bool StartAcquisition()
        {
           ADS.WriteBoolToPLC(true);
           acqStarted = ADS.StartAcquisition();
           return acqStarted;
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            if (!acqStarted)
            {
                CurrentForce = ADS.GetForce() / MySettings.Load().Scaling;
            }
        }

        public double[] StopAcquisition()
        {
            acqStarted = false;

            ADS.WriteBoolToPLC(false);

            Tuple<List<TimeSpan>, List<Double>> data = ADS.StopAcquisition();

            data = clearData(data);
            
            Plot1.Update(data);

            if (MySettings.Load().LoggingEnabled)
            {
                Logging log = new Logging();
                log.WriteToFile(data);
            }
            // OnPropertyChanged("plot");

            double[] result;

            result = ProcessStrippability.Calculate(data);

            //if (MySettings.Load().TestType==TestType.Strippability)
            //{
            //    result = ProcessStrippability.Calculate(data);
            //}
            //else
            //{
            //    //result = ProcessPullout.Calculate(data);
            //}

            return result;
        }

        public Tuple<List<TimeSpan>, List<double>> clearData(Tuple<List<TimeSpan>, List<double>>  data)
        {

            List<double> cleanedData = new List<double>();
            for (int i = 0; i < data.Item1.Count(); i++)
            {
                if (i > 0 && data.Item1[i] == TimeSpan.Zero)
                {
                    cleanedData = new List<double>(data.Item2.GetRange(0, i));
                    break;
                }
            }

            Tuple<List<TimeSpan>, List<double>> cleanedTuple = new Tuple<List<TimeSpan>, List<double>>(data.Item1, cleanedData);

            return cleanedTuple;
        }

        public void Model_OnTaraPressed(object sender, TaraArgs e)
        {

           ADS.WriteTara();
            
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
