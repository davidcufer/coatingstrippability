﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Timers;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Threading;
using System.Diagnostics;
using CoatingStrippability.Settings;

namespace CoatingStrippability
{
    class Lloyd
    {
        SerialPort port = new SerialPort();
        List<byte[]> initSequence = new List<byte[]>
                                {new byte[]{0xBA, 0xCA},
                                 new byte[]{0XB5},
                                 new byte[]{0xBC,0xD2,0x71},
                                 new byte[]{0xBB},
                                 new byte[]{0xB9},
                                 new byte[]{0xB4},
                                 new byte[]{0xBD,0xd2,0x72},
                                 new byte[]{0xBE,0xd2,0x73 },
                                 new byte[]{0xB0},
                                 new byte[]{0xD0, 0x03},
                                 new byte[]{0xD5,0x09,0x00,0xD2,0x74},
                                 new byte[]{0xD1,0x01,0x34,0x34,0x37,0x41,0x30,0x30,0x30,0x30,0xD2,0x75},
                                 new byte[]{0xD3,0x01,0x00,0xD2,0x76},
                                 new byte[]{0xD1,0x00,0x34,0x35,0x39,0x43,0x34,0x30,0x30,0x30,0xD2,0x77},
                                 new byte[]{0xD3,0x00,0x04,0xD2,0x78},
                                 new byte[]{0xD1,0x04,0x34,0x34,0x37,0x41,0x30,0x30,0x30,0x30,0xD2,0x79},
                                 new byte[]{0xD3,0x04,0x00,0xD2,0x7A},
                                 new byte[]{0xD1,0x05,0x34,0x34,0x37,0x41,0x30,0x30,0x30,0x30,0xD2,0x7B},
                                 new byte[]{0xD3,0x05,0x00,0xD2,0x7C},
                                 new byte[]{0xD5,0x04,0x00,0xD2,0x7D},
                                 new byte[]{0xD5,0x07,0x00,0xD2,0x7E},
                                 new byte[]{0xD5,0x02,0x01,0xD2,0x70},
                                 new byte[]{0xD5,0x01,0x00,0xD2,0x71},
                                 new byte[]{0xD5,0x03,0x01,0xD2,0x72},
                                 new byte[]{0xD6,0x04,0xD2,0x73},
                                 new byte[]{0xD5,0x11,0x01,0xD2,0x74},
                                 new byte[]{0xD5,0x10,0x01,0xD2,0x75},
                                 new byte[]{0xCA,0x82,0xD2,0x76},
                                 new byte[]{0xD1,0x06,0x34,0x32,0x37,0x30,0x30,0x30,0x30,0x30,0xD2,0x77,0xD3,0x06,0x02,0xD2,0x78,0xCA,0xD1,0x00,0x34,0x32,0x43,0x38,0x30,0x30,0x30,0x30,0xD2,0x79,0xD3,0x00,0x04,0xD2,0x7A,0xCA,0xD5,0x19,0x00,0xD2,0x7B,0xD1,0x0F,0x34,0x31,0x32,0x30,0x30,0x30,0x30,0x30,0xD2,0x7C,0xC0},
                                 new byte[]{0xD1,0x06,0x34,0x32,0x34,0x38,0x30,0x30,0x30,0x30}};
        byte[] periodicSequence = new byte[] { 0xD2, 0x7F };
        int initCounter = 0;
        int maxInitCounter = 29; //last command is setting speed to 50mm/min
        public Queue<byte> forceStream = new Queue<byte>();
        private static System.Timers.Timer aTimer;

        byte[] STOP = new byte[] { 0x81 };
        byte[] FASTUP = new byte[] { 0x86 };
        byte[] FASTDOWN = new byte[] { 0x87 };
        byte[] SLOWUP = new byte[] { 0x84 };
        byte[] SLOWDOWN = new byte[] { 0x85 };
        byte[] TOORIGIN = new byte[] { 0xd2, 0x71, 0x83 };
        byte[] SPEED10MMIN = new byte[] { 0xD1, 0x06, 0x34, 0x32, 0x34, 0x38, 0x30, 0x30, 0x30, 0x30 };
        byte[] SPEED50MMIN = new byte[] { 0xD1, 0x06, 0x34, 0x31, 0x32, 0x30, 0x30, 0x30, 0x30, 0x30 };

        System.Timers.Timer timeOut = new System.Timers.Timer();


        // status code table:
        //0-before init, 
        //1-initialized, 
        //2-

        int status = 0;
        int newStatus = 0;

        System.Threading.Timer periodicTimer;
        long force = 0;

        public TextBlock outputText { get; set; } = new TextBlock();

        public void Initialize()
        {
            port.Close();
            port.PortName = MySettings.Load().ComPort;
            port.BaudRate = 9600;
            port.DataBits = 8;
            port.StopBits = StopBits.One;
            port.Parity = Parity.None;
            port.Handshake = Handshake.XOnXOff;
            port.Encoding = Encoding.UTF8;
            port.WriteTimeout = 3000;
            port.ReadTimeout = 3000;
            port.Open();
            port.DtrEnable = true;
            port.RtsEnable = true;
            port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
            port.WriteTimeout = 5000;

            timeOut.Interval = 5000;
            timeOut.AutoReset = false;
            timeOut.Elapsed += ResetInit;
            timeOut.Start();
            //port.Write("\u0017");
        }


        void port_DataReceived(object sender, SerialDataReceivedEventArgs args)
        {
            List<int> response = new List<int> { };
            int bytes = port.BytesToRead;
            //UpdateLog(bytes.ToString()+"\n");
            byte[] buffer = new byte[bytes];
            port.Read(buffer, 0, bytes);
            //UpdateLog("Received: " + string.Join(" ", buffer.Select(x => x.ToString("X2"))) + "\n");
            ProcessData(buffer);
        }

        bool port_SendByteArray(byte[] data)
        {

            try
            {
                if (port.CtsHolding)
                {
                    port.WriteTimeout = 2000;
                    port.Write(data, 0, data.Length);
                }
            }
            catch (Exception timeout)
            {
                return false;
            }

            UpdateLog("Sent: " + string.Join(" ", data.Select(x => x.ToString("X2"))) + "\n");
            return true;
        }

        void UpdateLog(String str)
        {
            //MainWindow mw = MainWindow.main;

            //mw.Dispatcher.Invoke(new Action(() =>
            //{
            //    mw.
            //}), DispatcherPriority.Background);
        }

        void UpdateForce(double force)
        {
            //MainWindow mw = MainWindow.main;

            //mw.Dispatcher.Invoke(new Action(() =>
            //{
            //    mw.tb_Output.Text = force.ToString();
            //}), DispatcherPriority.Background);
        }

        public bool JogDown()
        {
            bool timeout = port_SendByteArray(FASTDOWN);
            return timeout;
        }

        public bool JogUp()
        {
            bool timeout = port_SendByteArray(FASTUP);
            return timeout;
        }

        public bool StartSlowUp()
        {
            bool timeout = port_SendByteArray(SLOWUP);
            return timeout;
        }
        public bool SetSpeed(double speed)
        {
            bool timeout = false;
            if (speed == 10.0)
            {
                //timeout = port_SendByteArray(SPEED50MMIN);
                //timeout = port_SendByteArray(SPEED10MMIN);
            }
            else
            {
                //timeout = port_SendByteArray(SPEED10MMIN);
                //timeout = port_SendByteArray(SPEED50MMIN);
            }
            return timeout;
        }

        public void StartJogDown()
        {
            port_SendByteArray(FASTDOWN);
        }

        public void StopMovement()
        {
            port_SendByteArray(STOP);
        }

        public void ResetInit(Object source, ElapsedEventArgs e)
        {
            status = 0;
            initCounter = 0;
        }

        public void ProcessData(byte[] buffer)
        {

            switch (status)
            {
                // initialization
                case 0:
                    if (initCounter <= maxInitCounter)
                    {
                        Thread.Sleep(100);
                        port_SendByteArray(initSequence[initCounter]);
                        initCounter++;
                    }
                    else
                    {
                        //SetTimer();
                        newStatus = 1;
                        timeOut.Start();
                    }
                    break;

                case 1:
                    timeOut.Stop();
                    timeOut.Start();

                    break;
                default:
                    break;
            }
            status = newStatus;

        }


        //void SetTimer()
        //{
        //    // Create a timer with a two second interval.
        //    aTimer = new System.Timers.Timer(1000);
        //    // Hook up the Elapsed event for the timer. 
        //    aTimer.Elapsed += OnTimedEvent;
        //    aTimer.AutoReset = true;
        //    aTimer.Enabled = true;
        //}


        //void OnTimedEvent(Object source, ElapsedEventArgs e)
        //{
        //    port_SendByteArray(periodicSequence);
        //}


    }
}
