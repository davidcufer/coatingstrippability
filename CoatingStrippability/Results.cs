﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace CoatingStrippability
{
    public class Results : INotifyPropertyChanged
    {
        private int _index;
        private double _max;
        private double _min;
        private double _median;
        private double _firstPeak;
        private double _stdev;

        public int Index
        {
            get
            {
                return _index;
            }
            set
            {
                _index = value;
                OnPropertyChanged("Index");
            }
        }

        public double Max
        {
            get
            {
                return _max;
            }
            set
            {
                _max = value;
                OnPropertyChanged("Max");
            }
        }

        public double Min
        {
            get
            {
                return _min;
            }
            set
            {
                _min = value;
                OnPropertyChanged("Min");
            }
        }


        public double Median
        {
            get
            {
                return _median;
            }
            set
            {
                _median = value;
                OnPropertyChanged("Median");
            }
        }

        public double FirstPeak
        {
            get
            {
                return _firstPeak;
            }
            set
            {
                _firstPeak = value;
                OnPropertyChanged("FirstPeak");
            }
        }

        public double StDev
        {
            get
            {
                return _stdev;
            }
            set
            {
                _stdev = value;
                OnPropertyChanged("FirstPeak");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
