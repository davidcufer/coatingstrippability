﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using CoatingStrippability.Settings;

namespace CoatingStrippability
{
    static class ProcessStrippability
    {
        public static double[] Calculate(Tuple<List<TimeSpan>, List<double>> data)
        {
            Tuple<TimeSpan, double> firstPeak = GetFirstPeak(data);
            Tuple<TimeSpan, double> lastPeak = GetLastPeak(data);

            double max = CalculateMax(data.Item2);
            double localMin = CalculateMin(data, firstPeak.Item1, lastPeak.Item1);
            double median = GetMedian(data, firstPeak.Item1, lastPeak.Item1);
            double firstpeak = GetFirstPeak(data).Item2;
            double stdev = CalculateStDev(data, firstPeak.Item1, lastPeak.Item1);

            return new double[] { max, localMin, double.Parse(median.ToString()), firstpeak, stdev };
        }

        private static double CalculateMax(List<double> data)
        {
            double max = -1000;
            for(int i=0;i<data.Count; i++)
            {
                if (data[i] > max)
                {
                    max = data[i];
                }
            }

            return max;
        }


        private static double CalculateMin(Tuple<List<TimeSpan>, List<double>> data, TimeSpan start, TimeSpan end)
        {
            double min = 15000000;
            for (int i = 0; i < data.Item2.Count; i++)
            {
                if (data.Item2[i] < min  && data.Item1[i]>start && data.Item1[i]<end)
                {
                    min = data.Item2[i];
                }
            }

            return min;
        }

        private static double CalculateStDev(Tuple<List<TimeSpan>, List<double>> data, TimeSpan start, TimeSpan end)
        {
            double sum2 = 0;
            double sum= 0;
            double n = 0;
            for (int i = 0; i < data.Item2.Count; i++)
            {
                if (data.Item1[i] > start && data.Item1[i] < end)
                {
                    sum += (double)data.Item2[i];
                    sum2 += (double)data.Item2[i] * (double)data.Item2[i];
                    n++;
                }
            }

            if (n == 0)
            {
                n = 1;
            }
            return Math.Sqrt(((sum2 / n) - ((sum/n) * (sum/n))));
        }

        private static double GetMedian(Tuple<List<TimeSpan>, List<double>> data, TimeSpan start, TimeSpan end)
        {
            double[] array = data.Item2.ToArray();

            List<double> tempList = new List<double>();

            double threshold = MySettings.Load().Threshold;
            bool overThreshold = false;

            for (int i=0;i< array.Length; i++)
            {
                if (array[i] > threshold && data.Item1[i]>start)
                {
                    tempList.Add(array[i]);
                    overThreshold = true;
                }

                if(overThreshold && array[i] < threshold || data.Item1[i] > end)
                {
                    break;
                }
            }
            
            double[] tempArray = tempList.ToArray();

            int count = tempArray.Length;

            if (count == 0)
            {
                return 0;
            }

            Array.Sort(tempArray);

            double medianValue = 0;

            if (count % 2 == 0)
            {
                // count is even, need to get the middle two elements, add them together, then divide by 2
                double middleElement1 = tempArray[(count / 2) - 1];
                double middleElement2 = tempArray[(count / 2)];
                medianValue = (middleElement1 + middleElement2) / 2;
            }
            else
            {
                // count is odd, simply get the middle element.
                medianValue = tempArray[(count / 2)];
            }

            return medianValue;
        }

        private static Tuple<TimeSpan, double> GetFirstPeak(Tuple<List<TimeSpan>, List<double>> data)
        {
            double threshold = MySettings.Load().Threshold;
            bool startProcessing = false;
            double average;
            double localMax = 0;
            TimeSpan localTime = new TimeSpan();
            for(int i=4;i<data.Item2.Count;i++)
            {
                if(data.Item2[i] > threshold)
                {
                    startProcessing = true;
                }

                average = (data.Item2[i - 3] + data.Item2[i - 2] + data.Item2[i - 1]) / 3.0;

                if (startProcessing && average > data.Item2[i])
                {

                    localMax = new double[] { data.Item2[i - 3], data.Item2[i - 2], data.Item2[i-1], data.Item2[i] }.Max(); ;
                    localTime = data.Item1[i];
                    break;
                }

            }

            return new Tuple<TimeSpan, double>(localTime, localMax);
        }

        private static Tuple<TimeSpan, double> GetLastPeak(Tuple<List<TimeSpan>, List<double>> data)
        {
            double threshold = MySettings.Load().Threshold;
            bool startProcessing = false;
            double average;
            double localMax = 0;
            TimeSpan localTime = new TimeSpan();
            for (int i = data.Item2.Count-151; i >= 0; i--)
            {
                if (data.Item2[i] > threshold)
                {
                    startProcessing = true;
                }

                average = (data.Item2[i + 150] + data.Item2[i + 100] + data.Item2[i + 50]) / 3.0;

                if (startProcessing && average > data.Item2[i])
                {

                    localMax = new double[] { data.Item2[i + 150], data.Item2[i +100], data.Item2[i+50]}.Max(); ;
                    localTime = data.Item1[i];
                    break;
                }

            }

            return new Tuple<TimeSpan, double>(localTime, localMax);
        }

        public static double[] CalculateMean(DataTable resultsTable)
        {
            double[] means = new double[resultsTable.Columns.Count];
            double[] sums = new double[resultsTable.Columns.Count];

            for (int i = 0; i<resultsTable.Columns.Count; i++)
            {
                sums[i] = 0;
            }

            for (int i = 0; i < resultsTable.Columns.Count; i++)
            {
                for (int j = 0; j < resultsTable.Rows.Count; j++)
                {
                    sums[i] += Convert.ToDouble(resultsTable.Rows[j][i]);
                }
                means[i] = sums[i] / resultsTable.Rows.Count;
            }

 
            return means;
        }

    }

}
